const content = $('.container');
let url = window.location.href;
let prefix = '';

if (!url.endsWith('/') && !url.endsWith('index.html')) {
    prefix = '../';
}

content.before(
    `
    <div class="nav-container">
        <div class="nav-center">
            <a href="` + prefix + `">
                <img src="https://edu-image.nosdn.127.net/3310f128e53b406f94400f7ae6046db2.png?imageView&quality=100" alt="">
            </a>
            <div class="nav-tabs">
                <div class="nav-tab">
                    <a href="#" target="_top">
                        <span>课程</span>
                    </a>
                </div>
                <div class="nav-tab">
                    <a href="` + prefix + `html/universities.html" target="_top">
                        <span>学校</span>
                    </a>
                </div>
                <div class="nav-tab">
                    <a href="#" target="_top">
                        <span>学校云</span>
                    </a>
                </div>
                <div class="nav-tab">
                    <a href="#" target="_top">
                        <span>慕课堂</span>
                    </a>
                </div>
                <div class="nav-tab">
                    <a href="#" target="_top">
                        <span>下载APP</span>
                    </a>
                </div>
            </div>
            <div class="nav-search-container">
                <div class="nav-search">
                    <label>
                        <input type="text" placeholder="搜索感兴趣的课程">
                    </label>
                    <a href="#" class="search-icon">
                        <img src="` + prefix + `img/search-icon.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>`
);

content.after(
    `
    <div class="footer">
			<div class="footer-content">
				<div class="layout1">
					<div class="layout-left">
						<div class="logo"></div>
						<p class="margin-top grey text-size-12">由高教社联手网易推出，让每一个有提升愿望的用户能够学到中国知名高校的课程，并获得认证。</p></div>
					<div class="layout-right margin-left">
						<div class="item">
						    <h4>关于我们</h4>
							<div class="about">
							<a href="` + prefix + `html/about.html">关于我们</a>
							<a href="` + prefix + `html/universities.html">学校云</a>
							<a href="#">联系我们</a>
							<a href="#">常见问题</a>
							<a href="` + prefix + `html/feedback.html">意见反馈</a>
							<a href="#">法律条款</a></div>
						</div>
						<div class="item">
						<h4>关注我们</h4>
						<img src="` + prefix + `img/subscribe.png" alt=""></div>
						<div class="item"><h4>友情链接</h4>
							<div class="about"><a href="">网易云课堂</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="layout2"><p class="light-grey text-size-12">网上有害信息举报（涉未成年人）：网站 https://www.12377.cn 电话
				010-58581010 邮箱（涉未成年人） youdao_jubao@rd.netease.com</p>
				<p class="light-grey text-size-12">粤B2-20090191-26 | 京ICP备12020869号-2 | 京公网安备44010602000207</p>
				<p class="light-grey text-size-12">©2014-2021 icourse163.org</p></div>
		</div>
    `
);